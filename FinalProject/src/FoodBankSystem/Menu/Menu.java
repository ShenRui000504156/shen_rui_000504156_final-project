/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FoodBankSystem.Menu;

import FoodBankSystem.Supplier.Product.Product;
import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class Menu {
    private int id;
    private static int menuID;
    private ArrayList<MenuItem> menu;
    private String type;
    private String healthCondition;
    private String gender;
    private String state;
    private String season;
    
    public enum AgeGroup{}
    public enum Income{}

    public Menu() {
        id = menuID++;
        menu = new ArrayList<MenuItem>();
    }

    public int getId() {
        return id;
    }

    public ArrayList<MenuItem> getMenu() {
        return menu;
    }
    
    public MenuItem addMenuItem(Product p){
        MenuItem mi = new MenuItem(p);
        menu.add(mi);
        return mi;
    }
    
    public void removeMenuItem(MenuItem mi){
        menu.remove(mi);
    }
}
