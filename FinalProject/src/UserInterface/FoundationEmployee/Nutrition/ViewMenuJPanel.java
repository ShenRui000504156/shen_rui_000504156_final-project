/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.FoundationEmployee.Nutrition;

import FoodBankSystem.FoodBank.FoodBank;
import FoodBankSystem.Menu.Menu;
import FoodBankSystem.Menu.Menu.AgeGroup;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Rui
 */
public class ViewMenuJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewMenuJPanel
     */
        
    JPanel userProcessContainer;
    
    public ViewMenuJPanel(JPanel userProcessContainer,FoodBank foodbank) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userProcessContainer = userProcessContainer;
        popAllCombox();
        
    }
    private void popAllCombox()
    {
        typeCombox.removeAllItems();
        typeCombox.addItem("Basic");
        typeCombox.addItem("Nutritious");

        ageCombox.removeAllItems();        
        for(AgeGroup agegroup : Menu.AgeGroup.values())
        {
            ageCombox.addItem(agegroup);
        }                
        
        healthConditionCombox.removeAllItems();
        healthConditionCombox.addItem("");
        healthConditionCombox.addItem("");
        
        incomeCombox.removeAllItems();
        for (Menu.AvgIncome avgIncome : Menu.AvgIncome.values())
        {
            incomeCombox.addItem(avgIncome);
        }
        
        genderCombox.removeAllItems();
        genderCombox.addItem("Female");
        genderCombox.addItem("Male");
        
        popStatesCombox();
        
        seasonCombox.removeAllItems();
        seasonCombox.addItem("Spring");
        seasonCombox.addItem("Summer");
        seasonCombox.addItem("Fall");
        seasonCombox.addItem("Winter");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        menuTable = new javax.swing.JTable();
        updateMenuButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        ageCombox = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        genderCombox = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        typeCombox = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        healthConditionCombox = new javax.swing.JComboBox<>();
        incomeCombox = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        stateCombox = new javax.swing.JComboBox<>();
        seasonCombox = new javax.swing.JComboBox<>();
        menuIDLabel = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        idText = new javax.swing.JTextField();
        backButton = new javax.swing.JButton();
        searchButton = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel1.setText("View/Update Menu");

        menuTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Food Name", "Quantity"
            }
        ));
        jScrollPane2.setViewportView(menuTable);

        updateMenuButton.setText("Update Menu");
        updateMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateMenuButtonActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel3.setText("Search Menu By:");

        jLabel4.setText("Age");

        ageCombox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel5.setText("Gender");

        genderCombox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel6.setText("Type");

        typeCombox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel7.setText("Health Condition");

        jLabel8.setText("Income");

        healthConditionCombox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        incomeCombox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel10.setText("State");

        jLabel12.setText("Season");

        stateCombox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        seasonCombox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        menuIDLabel.setText("jLabel11");

        jLabel13.setText("ID");

        backButton.setText("<<Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(backButton)
                            .addComponent(menuIDLabel)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(updateMenuButton)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 443, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel5)
                                                .addComponent(jLabel4)
                                                .addComponent(jLabel6))
                                            .addGap(138, 138, 138))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel7)
                                                .addComponent(jLabel12)
                                                .addComponent(jLabel10)
                                                .addComponent(jLabel8))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel13)
                                        .addGap(168, 168, 168)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(typeCombox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(idText)
                                    .addComponent(ageCombox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(genderCombox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(healthConditionCombox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(incomeCombox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(stateCombox, 0, 123, Short.MAX_VALUE)
                                    .addComponent(seasonCombox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(searchButton)))
                .addContainerGap(45, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(idText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(typeCombox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(ageCombox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(genderCombox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(healthConditionCombox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(incomeCombox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stateCombox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(seasonCombox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(searchButton)
                .addGap(26, 26, 26)
                .addComponent(menuIDLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(updateMenuButton)
                .addGap(26, 26, 26)
                .addComponent(backButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed

    private void updateMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateMenuButtonActionPerformed
        UpdateMenuJPanel umjp = new UpdateMenuJPanel(userProcessContainer);
        userProcessContainer.add("umjp", umjp);

        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_updateMenuButtonActionPerformed

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        int id = Integer.parseInt(idText.getText());
        for ()
        {
            
        }
        
        
    }//GEN-LAST:event_searchButtonActionPerformed

private void popStatesCombox()
    {
        stateCombox.removeAllItems();
        
        stateCombox.addItem("Alabama");
        stateCombox.addItem("Alaska");
        stateCombox.addItem("Arizona");
        stateCombox.addItem("Arkansas");
        stateCombox.addItem("California");
        stateCombox.addItem("Colorado");
        stateCombox.addItem("Connecticut");
        stateCombox.addItem("Delaware");
        stateCombox.addItem("Florida");
        stateCombox.addItem("Georgia");
        stateCombox.addItem("Hawaii");
        stateCombox.addItem("Idaho");
        stateCombox.addItem("Illinois");
        stateCombox.addItem("Indiana");
        stateCombox.addItem("Iowa");
        stateCombox.addItem("Kansas");
        stateCombox.addItem("Kentucky");
        stateCombox.addItem("Louisiana");
        stateCombox.addItem("Maine");
        stateCombox.addItem("Maryland");
        stateCombox.addItem("Massachusetts");
        stateCombox.addItem("Michigan");
        stateCombox.addItem("Minnesota");
        stateCombox.addItem("Mississippi");
        stateCombox.addItem("Missouri");
        stateCombox.addItem("Montana");
        stateCombox.addItem("Nebraska");
        stateCombox.addItem("Nevada");
        stateCombox.addItem("New Hampshire");
        stateCombox.addItem("New Jersey");
        stateCombox.addItem("New Mexico");
        stateCombox.addItem("New York");
        stateCombox.addItem("North Carolina");
        stateCombox.addItem("North Dakota");
        stateCombox.addItem("Ohio");
        stateCombox.addItem("Oklahoma");
        stateCombox.addItem("Oregon");
        stateCombox.addItem("Pennsylvania");
        stateCombox.addItem("Rhode Island");
        stateCombox.addItem("South Carolina");
        stateCombox.addItem("South Dakota");
        stateCombox.addItem("Tennessee");
        stateCombox.addItem("Texas");
        stateCombox.addItem("Utah");
        stateCombox.addItem("Vermont");
        stateCombox.addItem("Virginia");
        stateCombox.addItem("Washington");
        stateCombox.addItem("West Virginia");
        stateCombox.addItem("Wisconsin");
        stateCombox.addItem("Wyoming");
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox ageCombox;
    private javax.swing.JButton backButton;
    private javax.swing.JComboBox<String> genderCombox;
    private javax.swing.JComboBox<String> healthConditionCombox;
    private javax.swing.JTextField idText;
    private javax.swing.JComboBox incomeCombox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel menuIDLabel;
    private javax.swing.JTable menuTable;
    private javax.swing.JButton searchButton;
    private javax.swing.JComboBox<String> seasonCombox;
    private javax.swing.JComboBox<String> stateCombox;
    private javax.swing.JComboBox<String> typeCombox;
    private javax.swing.JButton updateMenuButton;
    // End of variables declaration//GEN-END:variables
}
